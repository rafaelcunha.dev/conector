import { middyfy } from '../../libs/lambda';
import Axios from 'axios';

const handler = async (event) => {
  try {
    const body = event.body;
    const url = body.url;
    const method = body.method ? body.method.toLowerCase() : 'post';
    const headers = { 'authorization': event.headers['x-platform-authorization'] };
    const pathVariables = {};

    if (body.customHeaders) {

      const custom_header = body.customHeaders;
      const arr_custom = custom_header.split(';');
      for (let i = 0; i < arr_custom.length; i++) {
      const header = arr_custom[i].split(':');
      headers[header[0]] = header[1];
    }
     delete body.customHeaders;
    }

    if (body.pathVariables) {
      const customVariables = body.pathVariables;
      const arrCustom = customVariables.split(';');

      for (let i = 0; i < arrCustom.length; i++) {
        const variables = arrCustom[i].split(':');
        pathVariables[variables[0]] = variables[1];
      }
    }

    let formattedUrl = url;

    for (const key in pathVariables) {
      const value = pathVariables[key];
      const regex = new RegExp(`{${key}}`, 'g');
      formattedUrl = formattedUrl.replace(regex, value);
    }

    const regex = /\${(.*?)}/g;
    
    const replacedUrl = formattedUrl.replace(regex, (match, captured) => {
      const fieldName = captured.trim();
      return body[fieldName];
    });


    const queryParams = body.queryParams || {};
    const queryString = Object.entries(queryParams)
      .map(([key, value]) => `${encodeURIComponent(key)}=${encodeURIComponent(value)}`)
      .join('&');
    const finalUrl = queryString ? `${replacedUrl}?${queryString}`: replacedUrl;

    const responseData = await Axios.request({
      method: method,
      url: finalUrl,
      headers: headers,
    });

    let response = responseData.data;

    if (body.rootObject) {
      response = response[body.rootObject];
    }

    return {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(response),
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message),
    };
  }
};

export const main = middyfy(handler);