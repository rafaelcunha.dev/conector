/* eslint-disable  @typescript-eslint/no-explicit-any */
import { middyfy } from '../../libs/lambda';
import estados from './estados.json'
import cidades from './cidades.json'

const handler = async (event) => {

  const  { state, city } = event.body;
  let cities: any = [];
  
  if (!state && city) {
    cities = cidades.filter((item) => item.nome === city);
  } else if(state && !city) {
    const stateId = estados.find((item) => item.sigla === state || item.nome === state)?.id;
    cities = cidades.filter((item) => item.estadoId === stateId);
  } else if(state && city) {
    const stateId = estados.find((item) => item.sigla === state || item.nome === state)?.id;
    cities = cidades.filter((item) => item.estadoId === stateId && item.nome === city);
  } else {
      const stateLength = estados.length;
      const randomState = Math.floor(Math.random() * stateLength);
      const stateId = estados[randomState].id;
      cities = cidades.filter((item) => item.estadoId === stateId);
  }

  return {
    statusCode: 200,
    body: JSON.stringify(cities),
  };

};

export const main = middyfy(handler);