import { middyfy } from '../../libs/lambda';

import Axios from 'axios';

import StringMask from 'string-mask';

const PATH_GET_USER = '/platform/user/queries/getUser'

const PATH_ACTIVE_EMPLOYEE = '/hcm/analytics/queries/activeEmployeeFilter'

const PATH_EMPLOYEE = '/hcm/employeejourney/entities/employee';

let response;
let url;
let responseData;

const infoEmployee = async (event) => {
  try {    
    url = event.headers['x-platform-environment'] + PATH_GET_USER;

    responseData = await Axios.get(url, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });

    const user = responseData.data;

    url = event.headers['x-platform-environment'] + PATH_ACTIVE_EMPLOYEE;    
    
    responseData = await Axios.get(url, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });

    const dataActiveEmployee = responseData.data; 

    if (dataActiveEmployee && !dataActiveEmployee.value) {
      return {
        statusCode: 404,
        body: 'Não foi possível encontrar o colaborador vinculado ao usuário logado'
      };
    }

    if (dataActiveEmployee )
  
    url = event.headers['x-platform-environment'] + PATH_EMPLOYEE + `?filter=id='${dataActiveEmployee.value}'`;
    responseData = await Axios.get(url, {
      headers: {
        Authorization: event.headers['x-platform-authorization']
      }
    });

    const data = responseData.data;

    data.contents.forEach(employee => {
      let employeeType = '';

      switch (employee.employeeType) {
        case 'EMPLOYEE':
          employeeType = 'Empregado';
          break;
        case 'THIRDPARTY':
          employeeType = 'Terceiro';
          break;
        case 'PARTNER':
          employeeType = 'Parceiro';
          break;
        default:
          employeeType = '';        
          break;
      }
      
      let contractType = '';

      switch (employee.contractType) {
        case 'EMPLOYEE':
          contractType = 'Empregado';
          break;
        case 'MANAGER':
          contractType = 'Diretor';
          break;
        case 'FARM_WORKER':
          contractType = 'Trabalhador Rural';
          break;        
        case 'RETIRED':
          contractType = 'Aposentado';
          break;
        case 'TRAINEE':
          contractType = 'Estagiário';
          break;
        case 'APRENDIZ':
          contractType = 'Aprendiz';
          break;
        case 'FIXED_DUE_DATE':
          contractType = 'Prazo Determinado - Lei 9.601/98';
          break;
        case 'RETIRED_MANAGER':
          contractType = 'Diretor Aposentado';
          break;
        case 'PUBLIC_AGENT':
          contractType = 'Agente Público';
          break;
        case 'TEACHER':
          contractType = 'Professor';
          break;
        case 'COOPERATIVE_WORKER':
          contractType = 'Cooperado';
          break;
        case 'DOMESTIC_WORKER':
          contractType = 'Trabalhador Doméstico';
          break;
        case 'TEACHER_FIXED_DUE_DATE':
          contractType = 'Professor Prazo Determinado';
          break;
        default:
          contractType = '';        
          break;
      }

      let cpf = '';

      if (employee.person?.cpf) {
        cpf = employee.person?.cpf;
        const formatter = new StringMask('000.000.000-00');
        if (cpf.length < 11) {
          while (cpf.length < 11) {
            cpf = '0'+cpf;
          }
        }

        cpf = formatter.apply(cpf);
      }

      let cnpj = '';

      if (employee.employer?.headquarter?.cnpj) {
        cnpj = employee.employer?.headquarter?.cnpj;
        const formatter = new StringMask('00.000.000/0000-00');
        if (cnpj.length < 14) {
          while (cnpj.length < 14) {
            cnpj = '0'+cnpj;
          }
        }

        cnpj = formatter.apply(cnpj);
      }
      

      const employeeFormated = {
        employeeId: employee.id ? employee.id : '',
        username: user.username ? user.username : '',
        email: employee.emails && employee.emails.length > 0 ? employee.emails[0].email : '',
        contractType: contractType,
        hireDate: employee.hireDate ? employee.hireDate : '',
        employeeType: employeeType,
        registerNumber: employee.registerNumber ? employee.registerNumber.toString() : '',
        employerId: employee.employer && employee.employer.headquarter ? employee.employer.headquarter.id : '',
        employerNumemp: employee.employer && employee.employer.headquarter ? employee.employer?.headquarter.numemp : '',
        employerTradingName: employee.employer && employee.employer.headquarter ? employee.employer.headquarter.tradingName : '',
        employerCompanyName: employee.employer && employee.employer.headquarter ? employee.employer.headquarter.companyName : '',
        employerCnpj: cnpj,
        personId: employee.person? employee.person.id : '',
        personFullname: employee.person? employee.person.fullName : '',
        personCpf: cpf,
        jobPositionCode: employee.jobPosition? employee.jobPosition.codcar : '',
        jobPositionName: employee.jobPosition? employee.jobPosition.name : '',
        departmentCode: employee.department? employee.department.code : '',
        departmentName: employee.department? employee.department.name : '',
        costCenterCode: employee.costCenter ? employee.costCenter.codccu : '',
        costCenterName: employee.costCenter ? employee.costCenter.name : '',
        workShiftCode: employee.workShift ? employee.workShift.codesc : '',
        workShiftName: employee.workShift ? employee.workShift.name : ''
      };
      
      response = employeeFormated;
    });

    return {
      statusCode: responseData?.status || 200,
      body: JSON.stringify(response)
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };
  }
};

export const main = middyfy(infoEmployee);