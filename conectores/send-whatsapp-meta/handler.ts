import { middyfy } from '../../libs/lambda';
import axios from 'axios';
import { validatePhoneNumber } from '../../libs/utils';
const sendWhatsapp = async (event) => {
  try {
    const { destination, content, token, sender } = event.body;

    const url = `https://graph.facebook.com/v16.0/${sender}/messages`;

    const phoneNumber =  validatePhoneNumber(destination);

    const data = {
      "messaging_product": "whatsapp",
      "recipient_type": "individual",
      "to": phoneNumber,
      "type": "text",
      "text": {
        "preview_url": false,
        "body": content
      }
    };
    
    const result = await axios.post(url, data, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    });

    return {
      statusCode: 200,
      body: JSON.stringify(result.data)
    };

  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    }
  }
};

export const main = middyfy(sendWhatsapp);
