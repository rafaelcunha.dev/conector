import { middyfy } from '../../libs/lambda';
import Axios from 'axios';

const PATH = '/platform/user/queries/listUsers';

const listActiveUsers = async (event) => {
  const searchTerm = event?.body?.searchTerm ? event?.body?.searchTerm : ''; 
  const url = event?.headers['x-platform-environment'] + PATH;
  
  try {
    const body = {
      includeBlocked: false,
      searchTerm: searchTerm
    }
    
    const responseData = await Axios.post(url, body, {
      headers: { Authorization: event.headers["x-platform-authorization"] }
    });

    const response = {
      statusCode: 200,
      body: JSON.stringify(responseData.data)
    };

    return response;
  } catch (error) {
    console.warn('Error: ', error);
    const response = {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    };

    return response;
  }
};

export const main = middyfy(listActiveUsers);
