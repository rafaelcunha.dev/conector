/* eslint-disable  @typescript-eslint/no-explicit-any */
import { middyfy } from '../../libs/lambda';

import axios from 'axios';

const handler = async (event) => {

  if (event?.body?.cep) {

    const cep = event?.body?.cep?.replace('-', '');

    const response = await axios.get(`https://viacep.com.br/ws/${cep}/json`);

    return {
      statusCode: 200,
      body: JSON.stringify([{
        cep: response?.data?.cep,
        street: response?.data?.logradouro,
        complement: response?.data?.complemento,
        province: response?.data?.bairro,
        city: response?.data?.localidade,
        uf: response?.data?.uf,
        ddd: response?.data?.ddd,
        ibge: response?.data?.ibge
      }])
    };
  } else {
    const ceps = ['39402795', '57018538', '82320380', '89040040'];
    const findedCep = [] as any;
    for(const cep of ceps) {
      const response = await axios.get(`https://viacep.com.br/ws/${cep}/json`);
      findedCep.push({
        cep: response?.data?.cep,
        street: response?.data?.logradouro,
        complement: response?.data?.complemento,
        province: response?.data?.bairro,
        city: response?.data?.localidade,
        uf: response?.data?.uf,
        ddd: response?.data?.ddd,
        ibge: response?.data?.ibge
      });
    }
    return {
      statusCode: 200,
      body: JSON.stringify(findedCep)
    };
  }

};

export const main = middyfy(handler);