import axios from 'axios';
import { middyfy } from '../../libs/lambda';

const PATH_USER = '/platform/user/queries/listUsers';
const PATH_GED_DOC_VERSION = '/platform/ecm_ged/entities/document';
const PATH_SIGN = '/platform/ecm_ged/actions/saveEnvelope';
const PATH_GED_SIGN = '/platform/ecm_ged/actions/sendDraftToSign';
const MARK_AS_FAILED = '/platform/ecm_ged/actions/markEnvelopeBatchDocumentFailed';

const singFormDocument = async (event) => {
  try {

    const user = {
      name: event.body?.name,
      email: event.body?.email,
      phone: event.body?.phone
    }

    if (event.body?.user) {
      const responseData = await axios.post(event.headers['x-platform-environment'] + PATH_USER, {
        users: [event.body?.user]
      }, {
        headers: { Authorization: event.headers['x-platform-authorization'] }
      });

      if (responseData.data?.users?.length === 1) {
        user.email = responseData.data?.users[0].email;
        user.name = responseData.data?.users[0].fullName;
        user.phone = responseData.data?.users[0].phone;
      }
    }
    if (!user.email) {
      await axios.post(event.headers['x-platform-environment'] + MARK_AS_FAILED, {
        envelopeBatchId: event.body?.envelopeBatchId,
        documentId: event.body?.documentId,
        scheduleConectorId: event.body?.scheduleConectorId,
        errorLog: 'Não foi possível encontrar o usuário no nome do arquivo!'
      }, {
        headers: { Authorization: event.headers['x-platform-authorization'] }
      });
      throw new Error('Não foi possível encontrar o usuário no nome do arquivo!');
    }

    const documentVersion = await axios.get(event.headers['x-platform-environment'] + PATH_GED_DOC_VERSION + `/${event.body?.documentId}`, {
      headers: { Authorization: event.headers['x-platform-authorization'] }
    });

    const sign = await axios.post(event.headers['x-platform-environment'] + PATH_SIGN, {
      askGeolocation: event.body?.askGeolocation || 'DONT_ASK_LOCATION',
      daysToExpire: event.body?.daysToExpire || 120,
      daysToNotify: event.body?.daysToNotify || 0,
      name: `${event.body?.prefixEnvelopeName} ${user.name}`,
      envelopeBatchId: event.body?.envelopeBatchId,
      documentsVersions: [
        documentVersion.data?.publishedVersion?.id
      ],
      signers: [{
        email: user.email,
        name: user.name,
        signerType: 'MANDATORY',
        username: user.email,
      }],
      permissions: event.body?.permissions ? JSON.parse(event.body.permissions) : []
    }, {
      headers: { Authorization: event.headers['x-platform-authorization'] }
    });

    await axios.post(event.headers['x-platform-environment'] + PATH_GED_SIGN, {
      envelopeDraftId: sign.data?.envelopeDraftId,
      notifyUser: true,
      sendEmail: true
    }, {
      headers: { Authorization: event.headers['x-platform-authorization'] }
    });

    return {
      statusCode: 200,
      body: JSON.stringify({})
    };

  } catch (error) {
    console.log(error);
    if (error?.response?.data?.message) {
      await axios.post(event.headers['x-platform-environment'] + MARK_AS_FAILED, {
        envelopeBatchId: event.body?.envelopeBatchId,
        documentId: event.body?.documentId,
        scheduleConectorId: event.body?.scheduleConectorId,
        errorLog: error?.response?.data?.message || error.message
      }, {
        headers: { Authorization: event.headers['x-platform-authorization'] }
      });
      throw new Error(error?.response?.data?.message);
    }
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    }
  }
};

export const main = middyfy(singFormDocument);
