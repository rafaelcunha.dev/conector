import { middyfy } from '../../libs/lambda';
import { Twilio } from 'twilio';
import { validatePhoneNumber } from '../../libs/utils';

const sendWhatsapp = async (event) => {

  try {
    const { destination, accountSid, content, token, providerNumber } = event.body;
    const client = new Twilio(accountSid, token);

    let phoneNumber = validatePhoneNumber(destination);

    const match = phoneNumber.match(/^(\d{2})(\d{2})(\d{5})(\d{4})$/);

    if (match) {
      phoneNumber = match[1] + match[2] + match[3].slice(1) + match[4];
    }
  
    const data = await client.messages.create({
      body: content,
      from: `whatsapp:${providerNumber}`,
      to: `whatsapp:${phoneNumber}`
    })

    return {
      statusCode: 200,
      body: JSON.stringify(data)
    };

  } catch (error) {
    console.log(error);
    return {
      statusCode: error?.response?.status || 400,
      body: error?.response?.data ? JSON.stringify(error?.response?.data) : JSON.stringify(error.message)
    }
  }
};

export const main = middyfy(sendWhatsapp);
