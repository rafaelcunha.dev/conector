import axios from 'axios';
import { middyfy } from '../../libs/lambda';

const handler = async (event) => {

  if (!event?.body?.cep) {
    return {
      statusCode: 400,
      body: 'Informe um CEP'
    };  
  }

  const cep = event.body.cep;

  const detalhes = await axios.get(`https://viacep.com.br/ws/${cep}/json/`);

  return {
    statusCode: 200,
    body: JSON.stringify(detalhes.data)
  };
};

export const main = middyfy(handler);