import { middyfy } from '../../libs/lambda';

const handler = async (event) => {

  const body = event.body;

  const retorno = {
    nome: body.name
  };

  return {
    statusCode: 200,
    body: JSON.stringify(retorno)
  };
};

export const main = middyfy(handler);